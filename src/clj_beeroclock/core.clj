(ns clj-beeroclock.core
  (:require [clj-beeroclock.check :refer [is-beeroclock-now]]
            [ring.adapter.jetty :refer [run-jetty]]
            [hiccup.page :refer [html5]]
            [environ.core :refer [env]])
  (:gen-class))

(defn beeroclock-page []
  [:h1 (if (is-beeroclock-now)
         "It's Beer O'Clock! :D"
         "It's Not Beer O'Clock! D:")])

(defn page-wrapper [body]
  "Return page html for the given hiccup body contents."
  (html5
   [:head
    [:title "Is it beer o'clock?"]]
   [:body
    body]))

(defn handler [request]
  "Ring HTTP request handler."
  {:status 2000
   :headers {"Content-Type" "text/html"}
   :body (-> (beeroclock-page)
             page-wrapper)})

(defn -main [& [port]]
  "Entrypoint to start the webapp."
  (let [port (Integer. (or port (env :port) 8080))]
    (run-jetty handler {:port port})))
