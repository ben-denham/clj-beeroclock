(ns clj-beeroclock.check
  (:require [clj-time.core :as time]))

(defn is-beeroclock [date]
  "Returns true if the given Datetime object is within the bounds of
  beer o'clock."
  (let [is-friday (= (time/day-of-week date) 5)
        after-4pm (>= (time/hour date) 16)]
    (and is-friday after-4pm)))

(defn is-beeroclock-now []
  "Returns true if the current time is within the bounds of beer
  o'clock."
  (is-beeroclock (time/now)))
