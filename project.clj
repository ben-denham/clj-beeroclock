(defproject clj-beeroclock "0.1.0-SNAPSHOT"
  :description "Is it beer o'clock?"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.12.2"]
                 [environ "1.1.0"]
                 [hiccup "1.0.5"]
                 [ring/ring-core "1.5.0"]
                 [ring/ring-jetty-adapter "1.5.0"]]
  :plugins [[lein-cloverage "1.0.9"]]
  :main clj-beeroclock.core)
