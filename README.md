# clj-beeroclock

A Clojure web app for reporting if it is currently beer o'clock.

## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
